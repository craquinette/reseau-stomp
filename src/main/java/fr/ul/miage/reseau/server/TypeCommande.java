package fr.ul.miage.reseau.server;

public enum TypeCommande {
	CONNECT("CONNECT"),
	SEND("SEND"),
	SUBSCRIBE("SUBSCRIBE"),
	UNSUBSCRIBE("UNSUBSCRIBE"),
	DISCONNECT("DISCONNECT"),
	ERROR("ERROR"),
	MESSAGE("MESSAGE"),
	RECEIPT("RECEIPT"),
	CONNECTED("CONNECTED"),
	STOMP("STOMP");

	private String name;

	public static boolean contains(String data) {
		boolean find = false;
		for (TypeCommande type : TypeCommande.values()) {
			find = data.equals(type.toString());
			if (find)
				break;
		}
		return find;
	}

	public static String getEnumByString(String code) {
		for (TypeCommande e : TypeCommande.values()) {
			if (e.name.equals(code))
				return e.name();
		}
		return null;
	}

	private TypeCommande(String val) {
        name = val;
    }

	@Override
	public String toString() {
		return name;
	}
}
