package fr.ul.miage.reseau.server;

import fr.ul.miage.reseau.model.StompClient;
import fr.ul.miage.reseau.model.Topic;
import org.java_websocket.WebSocket;

import java.util.HashMap;
import java.util.Map;

public class Stomp {

    private int ID_MESSAGE = 0;
    static Stomp stomp;

    private final Map<String, Topic> registreTopics = new HashMap<>();
    private final Map<String, StompClient> registreClients = new HashMap<>();

    private Stomp() {

    }

    /**
     * Constructeur Singleton
     * @return instance de Stomp
     */
    static Stomp getInstance() {
        if (stomp == null)
            stomp = new Stomp();
        return stomp;
    }

    /**
     * Fonction principale qui va bifurquer sur la bonne fonction en fonction
     * de la frame.
     * @param client client qui a déclenché de l'événement
     * @param frame Frame parsée préalablement côté serveur
     */
    public void handleFrame(WebSocket client, Frame frame) {
        String identifiantClient = client.getRemoteSocketAddress().toString(); // /ip.ip.ip.ip:1234
        String commande = TypeCommande.getEnumByString(frame.getCommand()); // CONNECT, STOMP...
        if (commande == null)
            commande = "";


        if (!commande.isEmpty() && !registreClients.containsKey(identifiantClient) && (!commande.equals("CONNECT") && !commande.equals("STOMP"))) {
            Map<String, String> headers = new HashMap<>();
            headers.put("content-type", "text/plain");
            client.send(Frame.ParseRequete(TypeCommande.ERROR, headers, "Please use a `CONNECT` frame before using others frames"));
            return;
        }

        switch (commande) {
            case "CONNECT":
                onConnect(client, frame);
                break;
            case "STOMP":
                onStomp(client, frame);
                break;
            case "SEND":
                onSend(client, frame);
                break;
            case "SUBSCRIBE":
                onSubscribe(client, frame);
                break;
            case "UNSUBSCRIBE":
                onUnsubscribe(client, frame);
                break;
            case "DISCONNECT":
                onDisconnect(client, frame);
                break;
            default:
                onError(client, frame);
                break;
        }
    }

    /**
     * Traitement de l'événement "onConnect"
     * @param client client qui a déclenché de l'événement
     * @param frame Frame parsée préalablement côté serveur
     */
    // REQUIRED: accept-version, (host)
    public void onConnect(WebSocket client, Frame frame) {
        Map<String, String> headers = new HashMap<>();

        // From STOMP 1.1 and onwards, the CONNECT frame MUST include the accept-version header.
        if (!frame.getHeaders().containsKey("accept-version")) {
            errorHandler(client, "The `accept-version` header must be defined for the `connect` frame");
        }

        // On ne supporte que la version 1.2
        if (frame.getHeaders().get("accept-version").equals("1.2")) {
            client.send(Frame.ParseRequete(TypeCommande.CONNECTED, headers, ""));
            headers.put("version", "1.2");
            registreClients.put(client.getRemoteSocketAddress().toString(), new StompClient(client.getRemoteSocketAddress().toString(), client));
        } else {
            errorHandler(client, "Supported protocol version is 1.2");
        }
    }

    /**
     * Traitement de l'événement "onStomp"
     * @param client client qui a déclenché de l'événement
     * @param frame Frame parsée préalablement côté serveur
     */
    public void onStomp(WebSocket client, Frame frame) {
        // Même traitement que onConnect
        onConnect(client, frame);
    }

    /**
     * @param client client qui a déclenché de l'événement
     * @param frame Frame parsée préalablement côté serveur
     */
    public void onSend(WebSocket client, Frame frame) {
        Map<String, String> headers = new HashMap<>();

        if (!frame.getHeaders().containsKey("destination")) {
            errorHandler(client, "The `destination` header is required for the `SEND`` frame");
            return;
        }

        String destination = frame.getHeaders().get("destination");

        if (!registreTopics.containsKey(destination)) {
            errorHandler(client, "The topic does not exist.");
            return;
        }

        Topic topic = registreTopics.get(destination);
        headers.put("destination", destination);
        headers.put("content-type", "text/plain");

        // Traitement multi-send (à tous les abonnés)
        for (StompClient clientAbonne : topic.getListeAbonnes()) {
            headers.clear();
            for (String identifiantAbonnement : clientAbonne.getAbonnements().get(topic)) {
                headers.put("subscription", identifiantAbonnement);
                headers.put("message-id", Integer.toString(this.ID_MESSAGE++));
                clientAbonne.getSocket().send(Frame.ParseRequete(TypeCommande.MESSAGE, headers, frame.getBody()));
            }
        }

        handleReceipt(client, frame); // Y a t-il un receipt ?
    }

    /**
     * Traitement de l'événement "onSubscribe"
     * @param client client qui a déclenché de l'événement
     * @param frame Frame parsée préalablement côté serveur
     */
    // REQUIRED: destination, id
    public void onSubscribe(WebSocket client, Frame frame) {
        Map<String, String> headers = new HashMap<>();
        String identifiantClient = client.getRemoteSocketAddress().toString();
        StompClient stompClient = registreClients.get(identifiantClient);

        if (!frame.getHeaders().containsKey("destination")) {
            errorHandler(client, "The `destination` header is required for the `SUBSCRIBE` frame");
            return;
        }

        if (!frame.getHeaders().containsKey("id")) {
            errorHandler(client, "The `id` header is required for the `SUBSCRIBE` frame");
            return;
        }

        String idAbonnement = frame.getHeaders().get("id");

        if (stompClient.checkAbonnementExisteDeja(idAbonnement)) {
            headers.put("content-type", "text/plain");
            client.send(Frame.ParseRequete(TypeCommande.ERROR, headers, "The `id` header MUST be unique, different subscriptions MUST use different subscription identifiers"));
            return;
        }

        String destination = frame.getHeaders().get("destination");
        Topic topic;

        // On récupère le Topic ou on le crée en fonction de l'existance ou non
        if (!registreTopics.containsKey(destination)) {
            topic = new Topic(destination);
            registreTopics.put(destination, topic);
        } else {
            topic = registreTopics.get(destination);
        }

        topic.addClient(stompClient);
        stompClient.addAbonnement(topic, idAbonnement);
        handleReceipt(client, frame); // Y a t-il un receipt ?
    }

    /**
     * Traitement de l'événement "onUnsubscribe"
     * @param client client qui a déclenché de l'événement
     * @param frame Frame parsée préalablement côté serveur
     */
    public void onUnsubscribe(WebSocket client, Frame frame) {
        Map<String, String> headers = new HashMap<>();
        String identifiantClient = client.getRemoteSocketAddress().toString();
        StompClient stompClient = registreClients.get(identifiantClient);

        if (!frame.getHeaders().containsKey("id")) {
            errorHandler(client, "The `id` header is required for the `UNSUBSCRIBE` frame");
            return;
        }

        String idAbonnement = frame.getHeaders().get("id");

        if (!stompClient.checkAbonnementExisteDeja(idAbonnement)) {
            errorHandler(client, "The `id` header MUST match the subscription identifier of an existing subscription");
            return;
        }

        stompClient.removeSubscription(idAbonnement);
        handleReceipt(client, frame); // Y a t-il un receipt ?
    }

    /**
     * Traitement de l'événement "onDisconnect"
     * @param client client qui a déclenché de l'événement
     * @param frame Frame parsée préalablement côté serveur
     */
    public void onDisconnect(WebSocket client, Frame frame) {
        handleReceipt(client, frame); // Y a t-il un receipt ?
        String identifiantClient = client.getRemoteSocketAddress().toString();
        StompClient stompClient = registreClients.get(identifiantClient);
        stompClient.removeAllSubscriptions(); // On le désabonne complétement et on retire son existence de ce StompClient de la liste des abonnés du Topic
        registreClients.remove(identifiantClient);
        client.close(0, "WebSocket closed as demanded.");
    }

    /**
     * Traitement de l'événement "onError" - tout ce qui n'est pas une commande/ce qui est inconnu est traité par une erreur
     * @param client client qui a déclenché de l'événement
     * @param frame Frame parsée préalablement côté serveur
     */
    public void onError(WebSocket client, Frame frame) {
        Map<String, String> headers = new HashMap<>();
        String commande = frame.getCommand();

        headers.put("content-type", "text/plain");
        client.send(Frame.ParseRequete(TypeCommande.ERROR, headers, "The `" + commande + "` is unknown. Please, RTFM."));
    }

    /**
     * Traitement complémentaire pour envoyer un accusé de réception
     * @param client client qui a déclenché de l'événement
     * @param frame Frame parsée préalablement côté serveur
     */
    public void handleReceipt(WebSocket client, Frame frame) {
        if (frame.getHeaders().containsKey("receipt")) {
            Map<String, String> headers = new HashMap<>();
            headers.put("receipt-id", frame.getHeaders().get("receipt"));
            client.send(Frame.ParseRequete(TypeCommande.RECEIPT, headers, ""));
        }
    }

    /**
     * Procédure qui généréralise la gestion des frame serveur ERROR
     * @param client client qui a déclenché de l'événement
     * @param body message
     */
    public void errorHandler(WebSocket client, String body) {
        Map<String, String> headers = new HashMap<>();
        headers.put("content-type", "text/plain");
        client.send(Frame.ParseRequete(TypeCommande.ERROR, headers, body));
    }

    /**
     * Nettoyage des clients qui ne sont plus connectés
     */
    public void clearDeadClients() {
        Map<String, StompClient> registreTempo = new HashMap<>(registreClients);
        registreTempo.forEach((idClient, instanceClient) -> {
            if (instanceClient.getSocket().isClosing() || instanceClient.getSocket().isClosed()) {
                instanceClient.removeAllSubscriptions();
                registreClients.remove(instanceClient);
            }
        });
    }

    /**
     * Retirer le client du registre des clients
     * @param client client qui a déclenché de l'événement
     */
    public void removeClientRegistry(WebSocket client) {
        String identifiantClient = client.getRemoteSocketAddress().toString();

        if (registreClients.containsKey(identifiantClient)) {
            StompClient stompClient = registreClients.get(identifiantClient);
            stompClient.removeAllSubscriptions();
            registreClients.remove(identifiantClient);
        }
    }
}
