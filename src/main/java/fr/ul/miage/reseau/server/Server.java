package fr.ul.miage.reseau.server;

import java.net.InetSocketAddress;
import java.util.Scanner;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.slf4j.LoggerFactory;

public class 	Server extends WebSocketServer {

	private final Stomp stomp = Stomp.getInstance();
	public Server(InetSocketAddress address) {
		super(address);
	}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
		conn.send("Bah dis-donc Doudou, cela faisait longtemps que tu ne venais plus aux soirées...");
		broadcast( "Nouvel invité à la soirée : " + conn.getRemoteSocketAddress());
		System.out.println("▲ [log - message envoyé] Nouvel invité à la soirée : " + conn.getRemoteSocketAddress());
	}

	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
		System.out.println("Socket close avec " + conn.getRemoteSocketAddress() + ", code " + code + " - Informations supplémentairess : " + reason);
		System.out.println(conn);
		stomp.removeClientRegistry(conn);
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		
		Frame frame = Frame.unParseRequete(message);
		stomp.handleFrame(conn, frame);
		System.out.println("▼ [Message reçu] " + conn.getRemoteSocketAddress() + " : " + message);
	}

	@Override
	public void onError(WebSocket conn, Exception ex) {
		System.err.println("[log] Erreur de discussion avec " + conn.getRemoteSocketAddress()  + " : " + ex);
	}
	
	@Override
	public void onStart() {
		System.out.println("[log] Serveur démarré correctement, ça ronronne...");
	}

	public static void main(String[] args) {
		final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Server.class);
		LOGGER.trace("trace");
		String host = "localhost";
		Scanner sc = new Scanner(System.in);
		System.out.println("Choisir le port utilisé (ws://localhost:xxxx/) :");

		int port = sc.nextInt();

		WebSocketServer server = new Server(new InetSocketAddress(host, port));
		server.run();
	}
}