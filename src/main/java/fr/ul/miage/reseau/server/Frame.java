package fr.ul.miage.reseau.server;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Stream;

public class Frame {
    private final int LIMIT_NUMBER_FRAME = 20;
    private final int MAX_LENGTH_HEADER_LINES = 100;
    private static final int MAX_LENGTH_BODY = 140;

    private String command;
    private Map<String, String> headers;
    private String body = "";

    /**
     * Constructeur
     * @param command COMMAND d'une frame
     * @param headers en-têtes de la frame
     * @param body message de la frame (optionnel)
     */
    public Frame(String command, Map<String, String> headers, String body) {
        this.command = command;
        this.headers = headers;
        this.body = body;
    }

    /**
     * Formatte la FRAME de type SERVER à partir des arguments donnés et constituant une frame
     * @param command COMMAND d'une frame
     * @param headers en-têtes de la frame
     * @param body message de la frame (optionnel)
     * @return String résultat
     */
    public static String ParseRequete(TypeCommande command, Map<String, String> headers, String body) {
        String res = command == null ? TypeCommande.ERROR.toString() : command.toString().toUpperCase().strip();
        String commande = TypeCommande.getEnumByString(command.toString());
        res += "\n";
        if (commande != null || !commande.equals("ERROR")) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                if (entry.getKey().contains(":") || entry.getValue().contains(":")) {
                    res = TypeCommande.ERROR.toString() + "\n" + gestionErrorParse(body);
                    break;
                }
                res += entry.getKey();
                res += ":";
                res += entry.getValue();
                res += "\n";
            }

			if(!body.isEmpty() && body.isBlank()) {
				res += "content-length";
				res += ":";
				res += body.getBytes(StandardCharsets.UTF_8).length;
				res += "\n";
			}
        } else
            res += gestionErrorParse(body);

        if(!body.isEmpty() && body.isBlank()) {
            res += "content-length";
            res += ":";
            res += body.getBytes(StandardCharsets.UTF_8).length;
            res += "\n";
        }

        res += "\n";
		if(!body.isEmpty() && !body.isBlank()) {
			res += body + "\n";
		}
        res += "\0"; // null

        return res;
    }

    /**
     * Parsage spécifique aux erreurs
     * @param body message
     * @return String formattée
     */
    public static String gestionErrorParse(String body) {
        String res = "content-type:text/plain";
        res += "\n";
        res += "content-length:";
		res += Integer.toString(body.getBytes(StandardCharsets.UTF_8).length);
		res += "\n";

        return res;
    }

    /**
     * Inverse de ParseRequete
     * @param data
     * @return
     */
    public static Frame unParseRequete(String data) {
        data = data.strip();
        String[] lines = data.split("\n");

        String command = "";
        Map<String, String> headers = new HashMap<String, String>();
        StringBuilder body = new StringBuilder();

        if (lines.length > 1) {
            command = lines[0];
            int nbLine = 1;

            // Splittage des en-têtes
            while (lines.length > nbLine && lines[nbLine].length() > 0) {
                String[] splitedHeader = lines[nbLine].split(":");
                if (splitedHeader.length >= 2)
                    headers.put(splitedHeader[0].trim(), splitedHeader[1].trim());
                nbLine++;
            }

            // Sans StringBuilder, Java le grand transforme le caractère null en chaîne de caratères "null"
            // Langage de mort ce truc...
            for (int j = nbLine; j < lines.length; j++)
                body.append(lines[j]);

            StringJoiner joiner = new StringJoiner("");
            Stream.of(body.toString().split("\0")).forEach(joiner::add); // Retrait du charactère maudis
            body = new StringBuilder(joiner.toString()); // Horrible n'est-ce pas ?
        }

        return new Frame(command, headers, body.toString());
    }

    /**
     * Simple getter
     * @return nom de la commande
     */
    public String getCommand() {
        return command;
    }

    /**
     * Simple getter
     * @return Map des headers
     */
    public Map<String, String> getHeaders() {
        return headers;
    }

    /**
     * Message de la frame
     * @return String message
     */
    public String getBody() {
        return body;
    }

    /**
     * Fonction qui lance le teste pour les frames
     * @param args
     */
    public static void main(String[] args) {
        TypeCommande command = TypeCommande.SEND;
        Map<String, String> headers = new
                HashMap<String, String>();
        headers.put("destination", "/queue/a");
        headers.put("receipt", "message-12345");
        String body = "hello queue a";

        System.out.println(Frame.ParseRequete(command, headers, body));


        System.out.println(unParseRequete(
                "SEND\n" + "destination:/queue/a\n" + "receipt:message-12345\n" + "\n" + "messages\0").getHeaders());

    }

}
