package fr.ul.miage.reseau.model;

import org.java_websocket.WebSocket;

import java.util.*;

public class StompClient {
    private String name;
    private WebSocket socket;
    private Map<Topic, List<String>> abonnements = new HashMap<>();
    private HashSet<String> listeIdentifiantsAbonnements = new HashSet<>();

    /**
     * Constructeur
     * @param name (/ip.ip.ip.ip:1234)
     * @param socket socket
     */
    public StompClient(String name, WebSocket socket) {
        this.name = name;
        this.socket = socket;
    }

    /**
     * Simple getter
     * @return String qui vaut le nom du client (/ip.ip.ip.ip:1234)
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Simple getter
     * @return Map des abonnements
     */
    public Map<Topic, List<String>> getAbonnements() {
        return abonnements;
    }

    /**
     * Simple setter des abonnements.
     * @param abonnements
     */
    public void setAbonnements(HashMap<Topic, List<String>> abonnements) {
        this.abonnements = abonnements;
    }

    /**
     * Simple getter.
     * @return
     */
    public WebSocket getSocket() {
        return socket;
    }

    /**
     * Simple getter.
     * @param socket la socket du client
     */
    public void setSocket(WebSocket socket) {
        this.socket = socket;
    }

    /**
     * Simple vérification d'existence de l'identifiant de l'abonnement
     * dans la liste d'abonnements du client.
     * @param identifiantAbonnement
     * @return
     */
    public boolean checkAbonnementExisteDeja(String identifiantAbonnement) {
        return listeIdentifiantsAbonnements.contains(identifiantAbonnement);
    }

    /**
     * Ajout un identifiant relativement unique pour un client dans le hashset
     * et cet identifiant à la liste des abonnements (avec insertion ou
     * mise à jour si l'abonnement est déjà existant, car rien ne l'interdit dans
     * le protocole)
     * @param topic
     * @param identifiantAbonnement
     * @return le résultat booléen de l'ajout.
     */
    public boolean addAbonnement(Topic topic, String identifiantAbonnement) {
        if (this.checkAbonnementExisteDeja(identifiantAbonnement))
            return false;

        listeIdentifiantsAbonnements.add(identifiantAbonnement);

        if (abonnements.containsKey(topic)) {
            abonnements.get(topic).add(identifiantAbonnement);
        } else {
            abonnements.put(topic, new ArrayList<String>(Arrays.asList(identifiantAbonnement)));
        }


        return true;
    }

    /**
     * Procédure opérant le désabonnement total, utilisé pour le nettoyage avant la
     * suppression du client de nos registres.
     */
    public void removeAllSubscriptions() {
        HashSet<String> hsTemp = new HashSet<>(listeIdentifiantsAbonnements);

        for (String id : hsTemp) {
            this.removeSubscription(id);
        }
    }

    /**
     * Désabonnement d'un topic : on cherche dans la liste des abonnements
     * pour chaque topic l'identifiant donné en paramètre puis on le retire
     * du bon topic. Possiblement plusieurs abonnements pour un même topic.
     * @param identifiantAbonnement
     * @return
     */
    public boolean removeSubscription(String identifiantAbonnement) {
        if (!this.checkAbonnementExisteDeja(identifiantAbonnement))
            return false;

        Topic topic = null;
        Iterator<Map.Entry<Topic, List<String>>> it = abonnements.entrySet().iterator();
        boolean found = false;

        while (it.hasNext() && !found) {
            Map.Entry<Topic, List<String>> pair = it.next(); // Itérateur
            Iterator<String> i = pair.getValue().iterator(); // Liste des identifiants des abonnements

            while (i.hasNext()) {
                // On itère sur les identifiants des abonnements
                String identifiantItere = i.next();

                // Si c'est le bon, on a le bon topic !
                if(identifiantItere.equals(identifiantAbonnement)) {
                    topic = pair.getKey();

                    // S'il n'y a plus rien d'identifiants liées au topic, nous pouvons
                    // retirer l'instance courante (le client) des abonnés du topic.
                    if(pair.getValue().size() == 0)
                        topic.removeClient(this);


                    pair.getValue().remove(identifiantItere);
                    listeIdentifiantsAbonnements.remove(identifiantAbonnement);
                    break;
                }
            }
        }

        // Oh oh.
        if(topic == null) {
            return false;
        }

        if(!(this.getAbonnements().get(topic).size() >= 1))
            topic.getListeAbonnes().remove(this);

        return true;
    }
}
