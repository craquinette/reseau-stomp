package fr.ul.miage.reseau.model;

import java.util.HashSet;

public class Topic {
    private String name;
    private HashSet<StompClient> listeAbonnes = new HashSet<StompClient>();

    /**
     * Constructeur
     * @param name n'importe quelle String
     */
    public Topic(String name) {
        this.name = name;
    }

    /**
     * Simple getter
     * @return nom du topic
     */
    public String getName() {
        return name;
    }

    /**
     * Simple setter
     * @param name nom du topic
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Simple getter
     * @return HashSet des StompClient
     */
    public HashSet<StompClient> getListeAbonnes() {
        return listeAbonnes;
    }

    /**
     * Simple setter
     * @param listeAbonnes HashSet des StompClient
     */
    public void setListeAbonnes(HashSet<StompClient> listeAbonnes) {
        this.listeAbonnes = listeAbonnes;
    }

    /**
     * Ajout l'instance StompClient qui vient de s'abonner à la liste
     * des abonnés
     * @param client
     */
    public void addClient(StompClient client) {
        if(!listeAbonnes.contains(client))
            listeAbonnes.add(client);
    }

    /**
     * Retraite de l'instance StompClient de la liste des abonnées
     * @param client
     */
    public void removeClient(StompClient client) {
        if(listeAbonnes.contains(client))
            listeAbonnes.remove(client);
    }
}
