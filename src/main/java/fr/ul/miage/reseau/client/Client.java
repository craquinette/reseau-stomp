package fr.ul.miage.reseau.client;

import fr.ul.miage.reseau.server.Frame;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Scanner;

public class Client extends WebSocketClient {

	public Client(URI serverUri, Draft draft) {
		super(serverUri, draft);
	}

	public Client(URI URI) {
		super(URI);
	}

	@Override
	public void onOpen(ServerHandshake handshakedata) {
		String message = "Hello, it's me, Mario!";
		send(message);
		System.out.println("▲ [log - message envoyé] " + message);

		message = "CONNECT\n"
				+ "login: websockets\n"
		+ "passcode: rabbitmq\n"
		+ "nickname: anonymous\n"
		+ "\n\n\0";
		send(message);
		System.out.println("▲ [log - message envoyé] " + message);

		/*System.out.println("Vous rejoignez la fête...");
		Iterator<String> it = handshakedata.iterateHttpFields();

		while (it.hasNext()) {
			String s = it.next();
			System.out.println("Key :" + s);
			System.out.println("Value : " + handshakedata.getFieldValue(s));
		}*/
	}

	@Override
	public void onClose(int code, String reason, boolean remote) {
		System.out.println("[log] Socket fermée avec le code " + code + " - Informations supplémentaires :\n" + reason);
	}

	@Override
	public void onMessage(String message) {
		System.out.println("▼ [Message reçu] " + message);
	}

	@Override
	public void onError(Exception ex) {
		System.err.println("[log] Erreur : " + ex);
	}

	public static void main(String[] args) throws URISyntaxException, InterruptedException {
		WebSocketClient client = new Client(new URI("ws://localhost:1337/"));
		client.connect();
		//Thread.sleep(3000);
		// System.out.println("closing");
		// client.closeBlocking();

		Scanner sc = new Scanner(System.in);
		String msg;
		int idsub;
		int choix;
		String message;
		String topic;
		Frame f;
		boolean q = false;
		while (q == false) {
			System.out.println("Affichage menu :");
			System.out.println("---------------------");
			System.out.println("1 : Publish");
			System.out.println("2 : Subscribe");
			System.out.println("3 : Unsubscribe");
			System.out.println("4 : Quit");
			choix = sc.nextInt();
			try {
				
				switch (choix) {
				case 1:
					msg = "SEND\n";
					System.out.println("Vous avez choisi l'option publish, entrez votre topic :");
					topic = sc.next();
					System.out.println("Entrez ensuite votre message :");
					message = sc.next();
					msg+="id:"+message+"\n";
					msg+="destination:"+topic+"\n";
					msg+="\n";
					msg+=Character.MIN_VALUE;
					client.send(msg);
					break;
				case 2:
					msg = "SUBSCRIBE\n";
					System.out.println("Vous avez choisi l'option subscribe, choisissez un centre d'intérêt :");
					topic = sc.next();
					System.out.println("Choisissez ensuite l'id de votre abonnement :");
					idsub = sc.nextInt();
					msg+="id:"+idsub+"\n";
					msg+="destination:"+topic+"\n";
					msg+="\n";
					msg+=Character.MIN_VALUE;
					client.send(msg);
					break;
				case 3:
					msg = "UNSUBSCRIBE\n";
					System.out.println(
							"Vous avez choisi l'option unsubscribe, choisissez le centre d'intérêt qui ne vous intéresse plus :");
					topic = sc.next();
					msg+="id:"+topic+"\n";
					msg+="\n";
					msg+=Character.MIN_VALUE;
					client.send(msg);
					break;
				case 4:
					msg = "DISCONNECT\n";
					System.out.println("Vous quittez l'application");
					
					msg+="receipt:"+Math.random()*100+"\n";
					msg+="\n";
					msg+=Character.MIN_VALUE;
					
					client.send(msg);
					q = true;
					break;
				default:
					System.out.println("Veuillez effectuer un choix valide !");
					break;
				}
			} catch (Exception e) {
				System.out.println("Format invalide, veuillez entrer un entier : " + e);
			}
		}
	}
}