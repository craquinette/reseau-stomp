package fr.ul.miage.reseau.test;

import static org.junit.Assert.*;

import java.io.File;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import fr.ul.miage.reseau.server.Frame;

public class FrameTest {
	public List<String> lignes;
	
	@Before
	public void setUp() throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("ressources/frame-forgees.txt").getFile());
		 try {
	            URI uri = file.toURI();
	            List<String> lines = Files.readAllLines(Paths.get(uri),
	                    Charset.forName("UTF8"));

	            for (String line : lines) {
	                System.out.println(line);
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
		 
		this.lignes = Files.readAllLines(file.toPath());
	}

	@Test
	public void testParsage() {
		String[] assertt = lignes.get(0).split("|");
		String command="SEND";
		Map<String,String> headers = new HashMap<String,String>();
		headers.put("destination", "/queue/a");
		headers.put("receipt", "message-12345");
		String body = "hello queue a";

		//String result = Frame.parseRequete(command, headers, body);
		String objective = "SEND\n" +
				"destination:/queue/a\n" +
				"receipt:message-12345\n" +
				"\n" +
				"hello queue a^@\0";
		
		assertEquals(objective, "");
	}
	
	@Test
	public void testDeparsage() {
		fail("Not yet implemented");
	}
}
