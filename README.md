# Projet serveur Publish/Subscribe avec STOMP :

## Sujet du projet :

Le projet a pour but de créer un serveur implémentant le protocole STOMP qui permet un systeme de publish/subscribe,<br/> 
les abonnés pourront donc recevoir des messages sur les topics sur lesquels ils seront abonnés. Ils pourront également <br />
se désabonner des sujets qui ne les intéressent plus et publier sur les topics où ils souhaitent partager leurs messages.


## Lancement du projet :

* Il faut tout d'abord commencer par lancer la classe Server.java dans le package server. Au lancement, on nous demande le port que l'on souhaite utiliser pour le serveur.
* Ensuite il faut lancer, index.html (avec le paramètre url GET : ?port=1234) présent à la racine du projet, une fois sur l'index on peut effectuer les differentes commandes.
* Le port par défaut du client HTML est le port 1337.

